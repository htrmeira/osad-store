<%-- 
    Document   : list_products
    Created on : 19-Apr-2013, 18:29:34
    Author     : heitor
--%>

<%@page import="uk.ac.coventry.uni.osad.model.Product"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <body id="list_products_body">
        <table border="1" cellspacing="0" cellpadding="0" width="100%" style="border-color: white;">
            <%
                Object productsObjects = request.getAttribute("products");
                if (productsObjects != null) {
                    List<Product> products = (List<Product>) productsObjects;
                    for (Product product : products) {%>
            <tr>
                <td height="100" width="100%" valign="top">
                    <jsp:include page="product_info.jsp">
                        <jsp:param name="product_id" value="<%= product.getId()%>"/>
                        <jsp:param name="product_name" value="<%= product.getName()%>"/>
                        <jsp:param name="product_description" value="<%= product.getDescription()%>"/>
                        <jsp:param name="product_price" value="<%= product.getPrice()%>"/>
                        <jsp:param name="product_quantity" value="<%= product.getQuantity()%>"/>
                    </jsp:include>
                </td>
            </tr>
            <%}
                } else {
                    System.err.println("No product list to be shown");
                }
            %>
        </table>
    </body>
</html>
