<%-- 
    Document   : product_info_purchasable
    Created on : 22-Apr-2013, 17:11:34
    Author     : heitor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <body>
        <table width="100%" height="100%">
            <tr>
                <td height="20%" width="80%" valign="top">
                    <%= (String) request.getParameter("product_id")%> - 
                    <a href="/osad-store/visualize_product?id=<%= (String) request.getParameter("product_id")%>">
                        <font size="3" face="Arial, Georgia, Garamond">
                        <%= (String) request.getParameter("product_name")%>
                        </font>
                    </a>
                </td> 
                <td height="20%" width="20%" valign="top" align="right">
                    <form name="add_item_form" method="post" action="/osad-store/add_item?item_id=<%= (String) request.getParameter("product_id")%>">
                        <input align="right" type="submit" value="Add to basket" name="add_to_basket_button" />
                    </form>
                </td> 
            </tr>
            <tr>
                <td height="60%" width="100%" valign="top">
                    <font size="2" face="Arial, Georgia, Garamond">
                    <%= (String) request.getParameter("product_description")%>
                    </font>
                </td>
            </tr>
            <tr>
                <td></td>
                <td height="20%" width="100%" valign="bottom" align="right">
                    £ <%= (String) request.getParameter("product_price")%>
                </td>
            </tr>
        </table>
    </body>
</html>
