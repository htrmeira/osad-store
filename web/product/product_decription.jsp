<%-- 
    Document   : product
    Created on : 19-Apr-2013, 14:15:23
    Author     : heitor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ page import="uk.ac.coventry.uni.osad.model.Product" %>
<!DOCTYPE html>
<html>
    <%
        Product prod = (Product) request.getAttribute("product");
    %>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title><%= prod.getName()%></title>
    </head>
    <body id="product_description_body" style="background-color: white">
        <jsp:include page="/index_header" />
        <form name="index_form" action="." method="POST">
            <table align="center" height="100%" width="50%">
                <tr>
                    <td height="20%">
                        <font size="5" face="Arial, Georgia, Garamond">
                        <%= (String) prod.getName()%>
                        </font>
                    </td>
                    <td align="right" height="20%">
                        £ <%= prod.getPrice()%>
                    </td>
                </tr>
                <tr >
                    <td height="20%">
                        <font size="2" face="Arial, Georgia, Garamond">
                        Stock No: <%= prod.getId()%>
                        </font>
                    </td>
                    <td align="right" height="20%">
                        <font size="2" face="Arial, Georgia, Garamond">
                        Available: <%= prod.getQuantity()%>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td valign="top" height="400px">
                        <font size="2" face="Arial, Georgia, Garamond">
                        <%= prod.getDescription()%>
                        </font>
                    </td>
                </tr>
                <tr>
                    <td></td>
                    <td valign="bottom" align="right">
                        <input type="submit" value="Back" name="back_button" />
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
