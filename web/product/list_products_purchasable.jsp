<%-- 
    Document   : list_products_purchasable
    Created on : 22-Apr-2013, 17:08:44
    Author     : heitor
--%>

<%@page import="java.util.List"%>
<%@page import="uk.ac.coventry.uni.osad.model.Product"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table border="1" cellspacing="0" cellpadding="0" width="100%" style="border-color: white;">
            <%
                Object productsObjects = request.getAttribute("products");
                if (productsObjects != null) {
                    List<Product> products = (List<Product>) productsObjects;
                    for (Product product : products) {%>
            <tr>
                <td height="100" width="100%" valign="top">
                    <jsp:include page="product_info_purchasable.jsp">
                        <jsp:param name="product_id" value="<%= product.getId()%>"/>
                        <jsp:param name="product_name" value="<%= product.getName()%>"/>
                        <jsp:param name="product_description" value="<%= product.getDescription()%>"/>
                        <jsp:param name="product_price" value="<%= product.getPrice()%>"/>
                        <jsp:param name="product_quantity" value="<%= product.getQuantity()%>"/>
                    </jsp:include>
                </td>
            </tr>
            <%}
                } else {
                    System.err.println("No product list to be shown");
                }
            %>
        </table>
    </body>
</html>
