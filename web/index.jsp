<%-- 
    Document   : index
    Created on : 18-Apr-2013, 22:56:12
    Author     : Heitor Meira (demeloh@uni.coventry.ac.uk)
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Welcome to OSAD Store</title>
    </head>
    <body id="index_body" style="background-color: white">
        <!-- The header is based on the user status 
        (logged in, no logged or logged as administrator) -->
        <jsp:include page="index_header" />

        <!-- Logo of OSAD store -->
        <h1 align="center" id="logo" >
            <font size="20" style="color: black" face="Georgia, Arial, Garamond">
                OSAD Store
            </font>
        </h1>

        <!-- Products list -->
        <table width="100%">
            <TR>
                <TD width="*%"><jsp:include page="list_products"/></TD>
                <!-- the basket is based in the user status (logged or not logged) -->
                <% if(session.getAttribute("logged_username") != null) { %>
                    <jsp:include page="list_basket" />
                <% } %>
            </TR>
        </table>
    </body>
</html>
