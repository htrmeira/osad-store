<%-- 
    Document   : purchase_info
    Created on : 24-Apr-2013, 23:37:12
    Author     : heitor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OSAD -Purchases</title>
    </head>
    <body>
        <form name="purchase_form" action="/osad-store/finalize_purchase?id=<%= (String) request.getParameter("purchase_id")%>" method="POST">
            <tr>
                <td>
                    <font size="2" face="Arial, Georgia, Garamond">
                    <%= (String) request.getParameter("purchase_id")%>
                    </font>
                </td>
                <td>
                    <font size="2" face="Arial, Georgia, Garamond">
                    <%= (String) request.getParameter("purchase_timestamp")%>
                    </font>
                </td>
                <td>
                    <font size="2" face="Arial, Georgia, Garamond">
                    <a href="/osad-store/visualize_product?id=<%= (String) request.getParameter("purchase_product_id")%>">
                    <%= (String) request.getParameter("purchase_product_name")%>
                    </a>
                    </font>
                </td>
                <td>
                    <font size="2" face="Arial, Georgia, Garamond">
                    <%= (String) request.getParameter("purchase_quantity")%>
                    </font>
                </td>
                <td>
                    <font size="2" face="Arial, Georgia, Garamond">
                    <%= (String) request.getParameter("purchase_total")%>
                    </font>
                </td>
                <td>
                    <font size="2" face="Arial, Georgia, Garamond">
                    <a href="/osad-store/visualize_user?user_to_visualize=<%= (String) request.getParameter("purchase_username")%>">
                    <%= (String) request.getParameter("purchase_username")%>
                    </a>
                    </font>
                </td>
                <td>
                    <font size="2" face="Arial, Georgia, Garamond">
                    <%= (String) request.getParameter("purchase_product_price")%>
                    </font>
                </td>
                <td>
                    <font size="2" face="Arial, Georgia, Garamond">
                    <%= (String) request.getParameter("purchase_product_quantity")%>
                    </font>
                </td>
                <td>
                    <input type="submit" value="Execute" name="execute_purchase" />
                </td>
            </tr>
        </form>
    </body>
</html>
