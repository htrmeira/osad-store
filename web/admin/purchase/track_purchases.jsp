    <%@page import="uk.ac.coventry.uni.osad.model.Purchase"%>
<%-- 
    Document   : list_users
    Created on : 22-Apr-2013, 16:29:24
    Author     : heitor
--%>

<%@page import="uk.ac.coventry.uni.osad.model.User"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OSAD -Purchases</title>
    </head>
    <body align="center">
        <jsp:include page="/index_header" />
        
        <table width="60%" border="1" cellspacing="0" align="center" style="border-color: white">
            <tr style="background-color: black">
                <td>
                    <font color="white" size="3" face="Arial, Georgia, Garamond">
                    <b>Purchase No</b>
                    </font>
                </td>
                <td>
                    <font color="white" size="3" face="Arial, Georgia, Garamond">
                    <b>Date</b>
                    </font>
                </td>
                <td>
                    <font color="white" size="3" face="Arial, Georgia, Garamond">
                    <b>Product</b>
                    </font>
                </td>
                <td>
                    <font color="white" size="3" face="Arial, Georgia, Garamond">
                    <b>Quantity</b>
                    </font>
                </td>
                <td>
                    <font color="white" size="3" face="Arial, Georgia, Garamond">
                    <b>Total</b>
                    </font>
                </td>
                <td>
                    <font color="white" size="3" face="Arial, Georgia, Garamond">
                    <b>Buyer</b>
                    </font>
                </td>
                <td>
                    <font color="white" size="3" face="Arial, Georgia, Garamond">
                    <b>Price</b>
                    </font>
                </td>
                <td>
                    <font color="white" size="3" face="Arial, Georgia, Garamond">
                    <b>Available</b>
                    </font>
                </td>
                <td>
                    <font color="white" size="3" face="Arial, Georgia, Garamond">
                    <b>Execute</b>
                    </font>
                </td>
            </tr>

            <%
                Object usersObjects = request.getAttribute("purchases");
                if (usersObjects != null) {
                    List<Purchase> purchases = (List<Purchase>) usersObjects;
                    for (Purchase purchase : purchases) {
            %>
                    
            <jsp:include page="purchase_info.jsp">
                <jsp:param name="purchase_id" value="<%= purchase.getId() %>"/>
                <jsp:param name="purchase_quantity" value="<%= purchase.getQuantity() %>"/>
                <jsp:param name="purchase_total" value="<%= purchase.totalPrice() %>"/>
                <jsp:param name="purchase_timestamp" value="<%= purchase.getTimestamp().toString() %>"/>
                
                <jsp:param name="purchase_product_id" value="<%= purchase.getProduct().getId() %>"/>
                <jsp:param name="purchase_product_price" value="<%= purchase.getProduct().getPrice() %>"/>
                <jsp:param name="purchase_product_quantity" value="<%= purchase.getProduct().getQuantity() %>"/>
                <jsp:param name="purchase_product_name" value="<%= purchase.getProduct().getName() %>"/>
                
                <jsp:param name="purchase_username" value="<%= purchase.getUser().getUsername() %>"/>
            </jsp:include>

            <%}
                } else {
                    System.err.println("No product list to be shown");
                }
            %>
        </table>
    </body>
</html>
