<%-- 
    Document   : add_adimin
    Created on : 22-Apr-2013, 00:40:18
    Author     : heitor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Register New OSAD User</title>
    </head>
    <body>
        <jsp:include page="/index_header" />
        <%
            String error_msg = (String) request.getAttribute("error");
            if (error_msg != null) {
        %>
        <script>
            alert("<%= error_msg%>");
        </script>
        <%}%>
        <form name="register_form" method="post" action="/osad-store/register">
            <!-- Logo of OSAD store -->
            <h1 align="center" id="logo" >
                <font size="20" style="color: black" face="Georgia, Arial, Garamond">
                OSAD Store
                </font>
            </h1>
            <table align="center" border="1" cellspacing="0" style="border-color: #F0F0F0">
                <tr>
                    <td height="150px" >
                        <table>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Username
                                    </font>
                                </td>
                                <td>
                                    <input required type="text" name="username" value="<%= request.getAttribute("username") != null ? request.getAttribute("username") : ""%>" size="20" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Email
                                    </font>
                                </td>
                                <td>
                                    <input required type="text" name="email" value="<%= request.getAttribute("email") != null ? request.getAttribute("email") : ""%>" size="20" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Password
                                    </font>
                                </td>
                                <td>
                                    <input required type="password" name="password" value="" size="20" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Confirm Password
                                    </font>
                                </td>
                                <td>
                                    <input required type="password" name="password_confirmation" value="" size="20" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Is Administrator
                                    </font>
                                </td>
                                <td>
                                    <input type="checkbox" name="admin_checkbox" value="ON" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="150px" >
                        <font face="Arial, Georgia, Garamond">
                        <b>Enter your card details</b>
                        </font>
                        <table valign="top">
                            <tr>
                                <td>
                                    <select name="cards">
                                        <option>MasterCard</option>
                                        <option>Visa</option>
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Card Number
                                    </font>
                                </td>
                                <td><input required type="text" name="card_number" value="<%= request.getAttribute("card_number") != null ? request.getAttribute("card_number") : ""%>" size="30" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Expire date
                                    </font>
                                </td>
                                <td>
                                    <font face="Arial, Georgia, Garamond">month:</font> 
                                    <input required type="text" name="expire_month" value="<%= request.getAttribute("expire_month") != null ? request.getAttribute("expire_month") : ""%>" size="2" /> 
                                    <font face="Arial, Georgia, Garamond">year:</font> 
                                    <input required type="text" name="expire_year" value="<%= request.getAttribute("expire_year") != null ? request.getAttribute("expire_year") : ""%>" size="4" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    CSC
                                    </font>
                                </td>
                                <td><input required type="text" name="csc_number" value="<%= request.getAttribute("csc_number") != null ? request.getAttribute("csc_number") : ""%>" size="10" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font face="Arial, Georgia, Garamond">
                        <b>Billing Address</b>
                        </font>
                        <table>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Street
                                    </font>
                                </td>
                                <td><input required type="text" name="street" value="<%= request.getAttribute("street") != null ? request.getAttribute("street") : ""%>" size="35" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Postcode
                                    </font>
                                </td>
                                <td><input required type="text" name="postcode" value="<%= request.getAttribute("postcode") != null ? request.getAttribute("postcode") : ""%>" size="20" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    City
                                    </font>
                                </td>
                                <td><input required type="text" name="city" value="<%= request.getAttribute("city") != null ? request.getAttribute("city") : ""%>" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Country
                                    </font>
                                </td>
                                <td><input required type="text" name="country" value="<%= request.getAttribute("country") != null ? request.getAttribute("country") : ""%>" /></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td align="right">
                                    <input align="right" type="submit" value="Register" name="register_button" />
                                    <input type="reset" value="Clear" name="clear_button" />    
                                </td>
                            </tr>
                        </table>
            </table>
        </td>
    </tr>
</form>
</body>
</html>
