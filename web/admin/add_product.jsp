<%-- 
    Document   : product
    Created on : 19-Apr-2013, 14:15:23
    Author     : heitor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add New Product</title>
    </head>
    <body>
        <jsp:include page="/index_header" />
        
        <%
            String error_msg = (String) request.getAttribute("error");
            if (error_msg != null) {
        %>
        <script>
            alert("<%= error_msg%>");
        </script>
        <%}%>
        
        <form name="frm" method="post" action="/osad-store/add_product">
            <table align="center" valign="center">
                <tr>
                    <td>
                        <font color="black" size="2" face="Arial, Georgia, Garamond">
                        Name
                        </font>
                    </td>
                    <td>
                        <input required type="text" name="name" value="<%= request.getAttribute("name") != null ? request.getAttribute("name") : "" %>" size="65" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <font color="black" size="2" face="Arial, Georgia, Garamond">
                        Price
                        </font>
                    </td>
                    <td>
                        <input type="text" name="price" value="<%= request.getAttribute("price") != null ? request.getAttribute("price") : "" %>" size="5" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <font color="black" size="2" face="Arial, Georgia, Garamond">
                        Quantity
                        </font>
                    </td>
                    <td>
                        <input type="text" name="quantity" value="<%= request.getAttribute("quantity") != null ? request.getAttribute("quantity") : "" %>" size="5" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <font color="black" size="2" face="Arial, Georgia, Garamond">
                        Description
                        </font>
                    </td>
                    <td>
                        <textarea name="description" rows="10" cols="50" >
                         <%= request.getAttribute("description") != null ? request.getAttribute("description") : "" %>
                        </textarea>
                    </td>
                </tr>
                <tr align="right">
                    <td> 
                    </td>
                    <td align="right">
                        <input type="submit" value="Submit">
                        <input type="button" value="Cancel">
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
