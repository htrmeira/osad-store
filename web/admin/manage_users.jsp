<%-- 
    Document   : manager_users
    Created on : 22-Apr-2013, 16:14:09
    Author     : heitor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OSDA - Users</title>
    </head>
    <body>
        <jsp:include page="/index_header" />
        <jsp:include page="/list_users" />
    </body>
</html>
