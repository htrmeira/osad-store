<%-- 
    Document   : user_info
    Created on : 22-Apr-2013, 16:16:36
    Author     : heitor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <body>
    <tr>
        <td width="30%">
            <a href="/osad-store/visualize_user?user_to_visualize=<%= (String) request.getParameter("username")%>">
                <%= (String) request.getParameter("username")%>
            </a>
        </td>
        <td width="50%">
            <font color="black" size="2" face="Arial, Georgia, Garamond">
            <a href="mailto:<%= (String) request.getParameter("email")%>">
                <%= (String) request.getParameter("email")%>
            </a>
            </font>
        </td>
        <td width="20%">
            <font color="black" size="2" face="Arial, Georgia, Garamond">
            <%= (String) request.getParameter("is_admin")%>
            </font>
        </td>
    </tr>
</body>
</html>
