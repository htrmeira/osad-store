<%-- 
    Document   : list_users
    Created on : 22-Apr-2013, 16:29:24
    Author     : heitor
--%>

<%@page import="uk.ac.coventry.uni.osad.model.User"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body align="center">
        <table width="60%" border="1" cellspacing="0" align="center" style="border-color: white">
            <tr style="background-color: black">
                <td>
                    <font color="white" size="3" face="Arial, Georgia, Garamond">
                    <b>Users</b>
                    </font>
                </td>
                <td>
                    <font color="white" size="3" face="Arial, Georgia, Garamond">
                    <b>Email</b>
                    </font>
                </td>
                <td>
                    <font color="white" size="3" face="Arial, Georgia, Garamond">
                    <b>Is Administrator</b>
                    </font>
                </td>
            </tr>

            <%
                Object usersObjects = request.getAttribute("users");
                if (usersObjects != null) {
                    List<User> users = (List<User>) usersObjects;
                    for (User user : users) {
            %>
                    
            <jsp:include page="user_info.jsp">
                <jsp:param name="username" value="<%= user.getUsername() %>"/>
                <jsp:param name="is_admin" value="<%= user.isIsAdmin() %>"/>
                <jsp:param name="email" value="<%= user.getEmail() %>"/>
            </jsp:include>

            <%}
                } else {
                    System.err.println("No product list to be shown");
                }
            %>
        </table>
    </body>
</html>
