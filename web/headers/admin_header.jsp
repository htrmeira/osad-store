<%-- 
    Document   : admin_header
    Created on : 22-Apr-2013, 15:29:36
    Author     : heitor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <table width="100%">
        <tr>
            <td>
    <p align="left">
        <a href="/osad-store/index.jsp">Home</a>
        <a href="/osad-store/user/myaccount.jsp">My Account</a>
        <a href="/osad-store/admin/manage_users.jsp">Manage Users</a>
        <a href="/osad-store/admin/register_admin.jsp">Register</a>
        <a href="/osad-store/admin/add_product.jsp">Add Product</a>
        <a href="/osad-store/list_purchases">Track Purchases</a>
    </p>
    </td>
    <td>
    <p align="right">
        <a href="/osad-store/logout">Logout</a>
    </p>
    </td>
    </table>
    
</html>
