<%-- 
    Document   : product_info
    Created on : 19-Apr-2013, 18:34:59
    Author     : heitor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <body>
        <table width="100%" height="100%">
            <tr>
                <td height="20%" width="100%" valign="top">
                    <%= (String) request.getParameter("basket_product_id")%> - 
                    <a href="/osad-store/visualize_product?id=<%= (String) request.getParameter("basket_product_id")%>">
                        <font size="3" face="Arial, Georgia, Garamond">
                        <%= (String) request.getParameter("basket_product_name")%>
                        </font>
                    </a>
                </td> 
            </tr>
            <tr>
                <td height="40%" width="100%" valign="top" align="right">
                    <font size="2" face="Arial, Georgia, Garamond">
                    Quantity: <%= (String) request.getParameter("basket_item_quantity")%>
                    </font>
                </td>
            </tr>
            <tr>
                <td height="20%" width="100%" valign="bottom" align="right">
                    <font size="2" face="Arial, Georgia, Garamond">
                    Price: £ <%= (String) request.getParameter("basket_product_price")%>
                    </font>
                </td>
            </tr>
            <tr>
            <td height="20%" width="100%" valign="bottom" align="right">
                <font size="2" face="Arial, Georgia, Garamond">
                    Total: £ <%= (String) request.getParameter("basket_total_price")%>
                    </font>
                </td>
                </tr>
        </table>
    </body>
</html>
