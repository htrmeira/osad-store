<%-- 
    Document   : list_products
    Created on : 19-Apr-2013, 18:29:34
    Author     : heitor
--%>

<%@page import="uk.ac.coventry.uni.osad.model.BasketItem"%>
<%@page import="uk.ac.coventry.uni.osad.model.Product"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <body id="list_basket_body">
    <td width="30%" valign="top">
        <div style="width: 100%; max-height: 400px; overflow: auto">
            <table cellspacing="0" border="1" cellpadding="0" style="width: 100%; border-color: white; ">
                <%
                    Object productsObjects = request.getAttribute("items");
                    if (productsObjects != null) {
                        List<BasketItem> basketItems = (List<BasketItem>) productsObjects;
                        for (BasketItem basketItem : basketItems) {
                %>
                <tr>
                    <td width="100%">
                        <jsp:include page="/basket/basket_item_info.jsp">
                            <jsp:param name="basket_product_id" value="<%= basketItem.getProduct().getId()%>"/>
                            <jsp:param name="basket_item_quantity" value="<%= basketItem.getQuantity()%>"/>
                            <jsp:param name="basket_product_price" value="<%= basketItem.getProduct().getPrice()%>"/>
                            <jsp:param name="basket_product_name" value="<%= basketItem.getProduct().getName()%>"/>
                            <jsp:param name="basket_total_price" value="<%= basketItem.totalPrice()%>"/>
                        </jsp:include>
                    </td>
                </tr>
                <%}

                    } else {
                        System.err.println("No product list to be shown");
                    }
                %>
            </table>
        </div>
        <table width="100%" valign="top">
            <tr>
                <td align="left">
                    <font size="3" face="Arial, Georgia, Garamond">
                    <% if (request.getAttribute("items") != null) {%>
                    Quantity: <%= request.getAttribute("total_quantity")%> items
                    <% }%>
                    </font>
                </td>
                <td align="right">
                    <form name="basket_form" method="post" action="/osad-store/basket/basket.jsp">
                        <input type="submit" value="View Basket" name="view_basket_button" />
                    </form>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <font size="3" face="Arial, Georgia, Garamond">
                    <% if (request.getAttribute("items") != null) {%>
                    Total: £ <%= request.getAttribute("total_price")%>
                    <% }%>
                    </font>
                </td>
            </tr>
        </table>
    </body>
</html>
