<%-- 
    Document   : detailed_item_info
    Created on : 22-Apr-2013, 22:54:19
    Author     : heitor
--%>

<%@page import="uk.ac.coventry.uni.osad.model.BasketItem"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <body>
        <table width="100%" height="100%">
            <tr>
                <td height="20%" width="80%" valign="top">
                    <%= (String) request.getParameter("basket_product_id")%> - 
                    <a href="/osad-store/visualize_product?id=<%= (String) request.getParameter("basket_product_id")%>">
                        <font size="3" face="Arial, Georgia, Garamond">
                        <%= (String) request.getParameter("basket_product_name")%>
                        </font>
                    </a>
                </td> 
                <td width="20%"></td>
            </tr>
            <tr>
                <td width="80%"></td>
                <td height="20%" width="20%" valign="top" align="right">
                    <font size="2" face="Arial, Georgia, Garamond">
                    Quantity: <%= (String) request.getParameter("basket_item_quantity")%>
                    </font>
                </td>
            </tr>
            <tr>
                <td width="80%"></td>
                <td height="20%" width="20%" valign="bottom" align="right">
                    <font size="2" face="Arial, Georgia, Garamond">
                    Price: £ <%= (String) request.getParameter("basket_product_price")%>
                    </font>
                </td>
            </tr>
            <tr>
                <td height="20%"width="80%">
                    <form name="basket_item_form" action="/osad-store/basket/basket.jsp?id=<%= request.getParameter("basket_id") %>" method="POST">
                <input type="submit" value="Add" name="add_item_button" onclick="<% request.setAttribute("test_button", "test1"); %>" />
                <input type="submit" value="Remove" name="remove_one_item_button" onclick="<% request.setAttribute("test_button", "test2"); %>" />
                <input type="submit" value="Remove All" name="remove_item" onclick="<% request.setAttribute("test_button", "test3"); %>" />
            </form>
        </td>
        <td height="20%" width="20%" valign="bottom" align="right">
            <font size="2" face="Arial, Georgia, Garamond">
            Total: £ <%= (String) request.getParameter("basket_total_price")%>
            </font>
        </td>
    </tr>
</table>
</body>
</html>
