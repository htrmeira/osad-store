<%-- 
    Document   : basket
    Created on : 22-Apr-2013, 22:44:41
    Author     : heitor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OSAD - My Basket</title>
    </head>
    <body>
        <jsp:include page="/index_header" />
        <jsp:include page="/list_detailed_basket" />
    </body>
</html>
