<%-- 
    Document   : deta
    Created on : 22-Apr-2013, 22:50:07
    Author     : heitor
--%>

<%@page import="uk.ac.coventry.uni.osad.model.BasketItem"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <body>
        <table cellspacing="0" border="1" cellpadding="0" style="width: 100%; border-color: white; ">
            <%
                Object productsObjects = request.getAttribute("items");
                if (productsObjects != null) {
                    List<BasketItem> basketItems = (List<BasketItem>) productsObjects;
                    for (BasketItem basketItem : basketItems) {
                        System.out.println("[JSP] id=" + basketItem.getId() + "basketItem=" + basketItem);
            %>
            <tr>
                <td width="100%">
                    <jsp:include page="/basket/detailed_item_info.jsp">
                        <jsp:param name="basket_id" value="<%= basketItem.getId()%>"/>
                        <jsp:param name="basket_item_quantity" value="<%= basketItem.getQuantity()%>"/>
                        <jsp:param name="basket_product_id" value="<%= basketItem.getProduct().getId()%>"/>
                        <jsp:param name="basket_product_name" value="<%= basketItem.getProduct().getName()%>"/>
                        <jsp:param name="basket_product_price" value="<%= basketItem.getProduct().getPrice()%>"/>
                        <jsp:param name="basket_total_price" value="<%= basketItem.totalPrice()%>"/>
                        <jsp:param name="basket_product_quantity" value="<%= basketItem.getProduct().getQuantity()%>"/>
                    </jsp:include>
                </td>
            </tr>
            <%}
                } else {
                    System.err.println("No product list to be shown");
                }
            %>

        </table>
        <form name="basket_form" method="post" action="/osad-store/confirmation">
            <table width="100%">
                <tr width="100%">
                    <td align="right" width="100%">
                        <font size="3" face="Arial, Georgia, Garamond">
                        <% if (request.getAttribute("items") != null) {%>
                        Quantity: <%= request.getAttribute("total_quantity")%> items
                        <% }%>
                        </font>
                    </td>
                </tr>
                <tr width="100%">
                    <td align="right" width="100%">
                        <font size="3" face="Arial, Georgia, Garamond">
                        <% if (request.getAttribute("items") != null) {%>
                        Total: £ <%= request.getAttribute("total_price")%>
                        <% }%>
                        </font>
                    </td>
                </tr>
                <tr width="100%">
                    <td align="right" width="100%">
                        <input type="submit" value="Proceed to Chekout" name="checkout_button" />
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
