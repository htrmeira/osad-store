<%-- 
    Document   : new_user
    Created on : 20-Apr-2013, 01:51:19
    Author     : heitor
--%>

<%@page import="uk.ac.coventry.uni.osad.model.CreditCard"%>
<%@page import="uk.ac.coventry.uni.osad.model.Address"%>
<%@page import="uk.ac.coventry.uni.osad.model.User"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <%
            User user = (User) session.getAttribute("logged_username");
            if(user == null) {
                user = new User();
            }
            Address address = user.getAddress();
            if (address == null) {
                address = new Address();
            }
            CreditCard creditCard = user.getCreditcard();
            if (creditCard == null) {
                creditCard = new CreditCard();
            }
        %>
        <title>My Account - <%= user.getUsername() %></title>
    </head>
    <body>
        <jsp:include page="/index_header" />
        <%
            String error_msg = (String) request.getAttribute("error");
            if (error_msg != null) {
        %>
        <script>
            alert("<%= error_msg%>");
        </script>
        <%}%>
        <form name="register_form" method="post" action="/osad-store/myaccount">
            <!-- Logo of OSAD store -->
            <h1 align="center" id="logo" >
                <font size="20" style="color: black" face="Georgia, Arial, Garamond">
                OSAD Store
                </font>
            </h1>
            <table align="center" border="1" cellspacing="0" style="border-color: #F0F0F0">
                <tr>
                    <td height="150px" >
                        <table>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Username
                                    </font>
                                </td>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    <input required type="text" name="username" value="<%= user.getUsername() != null ? user.getUsername() : "" %>" size="20" readonly="readonly"/>
                                    </font>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Email
                                    </font>
                                </td>
                                <td>
                                    <input required type="text" name="email" value="<%= user.getEmail() != null ? user.getEmail() : "" %>" size="20" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Password
                                    </font>
                                </td>
                                <td>
                                    <input required type="password" name="password" value="<%= user.getPassword() != null ? user.getPassword() : "" %>" size="20" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Confirm Password
                                    </font>
                                </td>
                                <td>
                                    <input required type="password" name="password_confirmation" value="<%= user.getPassword() != null ? user.getPassword() : "" %>" size="20" />
                                </td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td height="150px" >
                        <font face="Arial, Georgia, Garamond">
                        <b>Enter your card details</b>
                        </font>
                        <table valign="top">
                            <tr>
                                <td>
                                    <select name="cards" value="<%= creditCard.getCardtype() != null ? creditCard.getCardtype() : "MasterCard" %>">
                                        <option>MasterCard</option>
                                        <option>Visa</option>
                                    </select>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Card Number
                                    </font>
                                </td>
                                <td><input required type="text" name="card_number" value="<%= creditCard.getCardnumber() != null ? creditCard.getCardnumber() : "" %>" size="30" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Expire date
                                    </font>
                                </td>
                                <td>
                                    <font face="Arial, Georgia, Garamond">month:</font> 
                                    <input required type="text" name="expire_month" value="<%= creditCard.getExpire_month() %>" size="2" /> 
                                    <font face="Arial, Georgia, Garamond">year:</font> 
                                    <input required type="text" name="expire_year" value="<%= creditCard.getExpire_year() %>" size="4" />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    CSC
                                    </font>
                                </td>
                                <td><input required type="text" name="csc_number" value="<%= creditCard.getCsc() %>" size="10" /></td>
                            </tr>
                        </table>
                    </td>
                </tr>
                <tr>
                    <td>
                        <font face="Arial, Georgia, Garamond">
                        <b>Billing Address</b>
                        </font>
                        <table>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Street
                                    </font>
                                </td>
                                <td><input required type="text" name="street" value="<%= address.getStreet() != null ? address.getStreet() : "" %>" size="35" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Postcode
                                    </font>
                                </td>
                                <td><input required type="text" name="postcode" value="<%= address.getPostcode() != null ? address.getPostcode() : "" %>" size="20" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    City
                                    </font>
                                </td>
                                <td><input required type="text" name="city" value="<%= address.getCity() != null ? address.getCity() : "" %>" /></td>
                            </tr>
                            <tr>
                                <td>
                                    <font face="Arial, Georgia, Garamond">
                                    Country
                                    </font>
                                </td>
                                <td><input required type="text" name="country" value="<%= address.getCountry() != null ? address.getCountry() : "" %>" /></td>
                            </tr>
                            <tr>
                                <td></td>
                                <td align="right">
                                    <input align="right" type="submit" value="Register" name="register_button" />
                                </td>
                            </tr>
                        </table>
            </table>
        </td>
    </tr>
</form>
</body>
</html>
