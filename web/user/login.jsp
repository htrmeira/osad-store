<%-- 
    Document   : login
    Created on : 22-Apr-2013, 00:17:35
    Author     : heitor
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <jsp:include page="/index_header" />
        
        <%
            String error_msg = (String) request.getAttribute("error");
            if (error_msg != null) {
        %>
        <script>
            alert("<%= error_msg %>");
        </script>
        <%}%>
        
        <!-- Logo of OSAD store -->
            <h1 align="center" id="logo" >
                <font size="20" style="color: black" face="Georgia, Arial, Garamond">
                OSAD Store
                </font>
            </h1>
        <form name="login_form" method="post" valign="center" action="/osad-store/login">
            <table align="center" valign="center" style="margin-top: 100px">
                <tr>
                    <td>
                        <font face="Arial, Georgia, Garamond">
                        Username
                        </font>
                    </td>
                    <td>
                        <input type="text" name="username" value="" size="20" />
                    </td>
                </tr>

                <tr>
                    <td>
                        <font face="Arial, Georgia, Garamond">
                        Password
                        </font>
                    </td>
                    <td>
                        <input type="password" name="password" value="" size="20" />
                    </td>
                </tr>
                <tr>
                    <td>
                        <font face="Arial, Georgia, Garamond">
                        Admin
                        </font>
                    </td>
                    <td>
                        <input type="checkbox" name="admin_checkbox" value="asAdmin" />
                    </td>
                </tr>
                <tr>
                    <td>
                    </td>
                    <td align="right">
                        <input type="submit" value="Login" name="login_button" />
                        <input type="submit" value="Cancel" name="cancel_button" />
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
