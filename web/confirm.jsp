<%-- 
    Document   : card_details
    Created on : 23-Apr-2013, 00:34:32
    Author     : heitor
--%>

<%@page import="uk.ac.coventry.uni.osad.model.CreditCard"%>
<%@page import="uk.ac.coventry.uni.osad.model.Address"%>
<%@page import="uk.ac.coventry.uni.osad.model.User"%>
<%@page import="uk.ac.coventry.uni.osad.model.BasketItem"%>
<%@page import="java.util.List"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>OSAD - Checkout</title>
    </head>
    <body >
        <jsp:include page="index_header" />
        <form name="purchase_form" action="execute_purchase" method="POST">
            <%
                User user = (User) session.getAttribute("logged_username");
                Address address = user.getAddress();
                if (address == null) {
                    address = new Address();
                }
                CreditCard creditCard = user.getCreditcard();
                if (creditCard == null) {
                    creditCard = new CreditCard();
                }
            %>
            <table align="center" width="80%">
                <tr align="center">
                    <td align="center">
                        <font size="5" face="Arial, Georgia, Garamond">
                        <b>Delivery Address:</b>
                        </font>
                        <table align="center" >
                            <tr>
                                <td align="left">
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <b>Street:</b>
                                    </font>
                                </td>
                                <td align="left">
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <%= address.getStreet()%>
                                    </font>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <b>Postcode:</b>
                                    </font>
                                </td>
                                <td>
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <%= address.getPostcode()%>
                                    </font>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <b>City:</b>
                                    </font>
                                </td>
                                <td>
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <%= address.getCity()%>
                                    </font>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <b>Country:</b>
                                    </font>
                                </td>
                                <td>
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <%= address.getCountry()%>
                                    </font>
                                </td>
                            </tr>
                        </table>
                    </td>
                    <%-- end of address --%>
                    <%-- begin of credit card --%>
                    <td>
                        <font size="5" face="Arial, Georgia, Garamond">
                        <b>Credit Card:</b>
                        </font>


                        <table align="center" >
                            <tr>
                                <td align="left">
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <b>Card Type:</b>
                                    </font>
                                </td>
                                <td align="left">
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <%= creditCard.getCardtype() %>
                                    </font>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <b>Card number:</b>
                                    </font>
                                </td>
                                <td align="left">
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <%= creditCard.getCardnumber()%>
                                    </font>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <b>Expire month:</b>
                                    </font>
                                </td>
                                <td>
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <%= creditCard.getExpire_month()%>
                                    </font>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <b>Expire year:</b>
                                    </font>
                                </td>
                                <td>
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <%= creditCard.getExpire_year()%>
                                    </font>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <b>CSC:</b>
                                    </font>
                                </td>
                                <td>
                                    <font size="3" face="Arial, Georgia, Garamond">
                                    <%= creditCard.getCsc()%>
                                    </font>
                                </td>
                            </tr>
                        </table>


                    </td>
                    <%-- end of credit card --%>
                    <%-- begin of basket list --%>
                <table width="60%" align="center" border=1 cellspacing="0" style="margin-top: 10px">
                    <tr>
                        <td width="70%" bgcolor="black" align="center" >
                            <font style="color: white" size="3" face="Arial, Georgia, Garamond">
                            Product
                            </font>
                        </td>
                        <td width="10%" bgcolor="black" align="center" >
                            <font style="color: white" size="3" face="Arial, Georgia, Garamond">
                            Quantity
                            </font>
                        </td>
                        <td width="10%" bgcolor="black" align="center" >
                            <font style="color: white" size="3" face="Arial, Georgia, Garamond">
                            Price
                            </font>
                        </td>
                        <td width="10%" bgcolor="black" align="center" >
                            <font style="color: white" size="3" face="Arial, Georgia, Garamond">
                            Total
                            </font>
                        </td>
                    </tr>
                    <%
                        Object productsObjects = request.getAttribute("items");
                        if (productsObjects != null) {
                            List<BasketItem> basketItems = (List<BasketItem>) productsObjects;
                            for (BasketItem basketItem : basketItems) {
                    %>
                    <tr>
                        <td>
                            <font style="color: black" size="2" face="Arial, Georgia, Garamond">
                            <%= basketItem.getProduct().getName()%>
                            </font>
                        </td>
                        <td align="center" >
                            <font style="color: black" size="2" face="Arial, Georgia, Garamond">
                            <%= basketItem.getQuantity()%>
                            </font>
                        </td>
                        <td align="center" >
                            <font style="color: black" size="2" face="Arial, Georgia, Garamond">
                            £ <%= basketItem.getProduct().getPrice()%>
                            </font>
                        </td>
                        <td align="center" >
                            <font style="color: black" size="2" face="Arial, Georgia, Garamond">
                            £ <%= basketItem.totalPrice()%>
                            </font>
                        </td>
                    </tr>
                    <%}
                        } else {
                            System.err.println("No product list to be shown");
                        }
                    %>
                </table>
                <%-- end of basket list --%>
                </tr>
                <tr>
                    <td>

                        <%-- begin of totals --%>
                        <table align="center" width="60%" >
                            <tr align="right" width="100%">
                                <td width="80%"></td>
                                <td width="10%" align="right">
                                    <font style="color: black" size="3" face="Arial, Georgia, Garamond">
                                    <b>Quantity:</b>
                                    </font>
                                </td>
                                <td width="10%" align="right">
                                    <font style="color: black" size="3" face="Arial, Georgia, Garamond">
                                    <% if (request.getAttribute("items") != null) {%>
                                    <%= request.getAttribute("total_quantity")%> items
                                    <% }%>
                                    </font>
                                </td>
                            </tr>

                            <tr align="right" width="100%">
                                <td width="80%"></td>
                                <td width="10%" align="right">
                                    <font style="color: black" size="3" face="Arial, Georgia, Garamond">
                                    <b>Total:</b>
                                    </font>
                                </td>
                                <td width="10%" align="right">
                                    <font style="color: black" size="3" face="Arial, Georgia, Garamond">
                                    <% if (request.getAttribute("items") != null) {%>
                                    £ <%= request.getAttribute("total_price")%>
                                    <% }%>
                                    </font>
                                </td>
                            </tr>
                            
                            
                            
                            <tr align="right" width="100%">
                                <td width="80%"></td>
                                <td width="10%" align="right">
                                </td>
                                <td width="10%" align="right">
                                    <input type="submit" value="Confirm" name="confirm_button" />
                                </td>
                            </tr>
                            
                            
                        </table>
                        <%-- end of totals --%>
                    </td>
                </tr>
            </table>
        </form>
    </body>
</html>
