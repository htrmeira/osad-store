/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.coventry.uni.osad.controller.store;

import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import org.junit.Ignore;
import uk.ac.coventry.uni.osad.model.Address;
import uk.ac.coventry.uni.osad.model.BasketItem;
import uk.ac.coventry.uni.osad.model.CreditCard;
import uk.ac.coventry.uni.osad.model.Product;
import uk.ac.coventry.uni.osad.model.Purchase;
import uk.ac.coventry.uni.osad.model.User;
import uk.ac.coventry.uni.osad.util.OsadException;

/**
 *
 * @author heitor
 */
public class StoreTest {
    private static SingletonStore store;
    private static Product product1;
    private static User user1;
    
    public StoreTest() { }
    
    @BeforeClass
    public static void setUpClass() {
        store = SingletonStore.instance();
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() throws Exception {
        product1 = createProduct(0);
        user1 = createUser(0);
    }
    
    @After
    public void tearDown() {
        store.removePurchases(user1.getUsername());
        store.removeItemsFromBasket(user1.getUsername());
        store.removeUser(user1.getUsername());
        store.removeProduct(String.valueOf(product1.getId()));
    }
    
    @Test
    public void purchaseTest() throws OsadException {
        store.addNewUser(user1);
        product1 = createNewProduct(product1);

        store.addItemToBasket(user1.getUsername(), String.valueOf(product1.getId()));
        BasketItem besketItem = store.getBasketItem(user1.getUsername(), String.valueOf(product1.getId()));
        assertEquals(1, besketItem.getQuantity());
        
        System.out.println("============== 1 ===============");
        List<Purchase> purchase1 = store.getPurchases(user1.getUsername());
        store.executePurchase(user1.getUsername());
        List<Purchase> purchase2 = store.getPurchases(user1.getUsername());
        
        assertEquals("should have one item more", purchase1.size() + 1, purchase2.size());
        
        for(Purchase product : purchase1) {
            purchase2.remove(product);
        }
        
        assertEquals("should have only one item", purchase2.size(), 1);
        
        Purchase purchase = purchase2.get(0);
        
        assertEquals(purchase.getUser(), user1);
        assertEquals(purchase.getProduct(), product1);
        assertEquals(purchase.getQuantity(), besketItem.getQuantity());
        
        assertNull(store.getBasketItem(user1.getUsername(), String.valueOf(product1.getId())));
        
        assertTrue(store.removePurchases(user1.getUsername()));
        
        System.out.println("============== 2 ===============");
    }
    
    private Product createNewProduct(Product productToCreate) {
        List<Product> products1 = store.getProducts();
        store.addNewProduct(productToCreate);
        List<Product> products2 = store.getProducts();
        
        for(Product product : products1) {
            products2.remove(product);
        }
        
        return products2.get(0);
    }
    
    @Test
    public void basketTest() throws OsadException {
        store.addNewUser(user1);
        List<Product> products1 = store.getProducts();
        store.addNewProduct(product1);
        List<Product> products2 = store.getProducts();
        
        for(Product product : products1) {
            products2.remove(product);
        }
        
        product1 = products2.get(0);
        
        store.addItemToBasket(user1.getUsername(), String.valueOf(product1.getId()));
        BasketItem besketItem = store.getBasketItem(user1.getUsername(), String.valueOf(product1.getId()));
        assertEquals(1, besketItem.getQuantity());
        store.addItemToBasket(user1.getUsername(), String.valueOf(product1.getId()));
        
        besketItem = store.getBasketItem(user1.getUsername(), String.valueOf(product1.getId()));
        assertEquals(product1, besketItem.getProduct());
        assertEquals(2, besketItem.getQuantity());
        assertEquals(user1.getUsername(), besketItem.getUsername());
        
        List<BasketItem> basket = store.getBasketItems(user1.getUsername());
        assertEquals(1, basket.size());
        assertEquals(besketItem, basket.get(0));
        
        store.removeItemFromBasket(user1.getUsername(), String.valueOf(product1.getId()));
        besketItem = store.getBasketItem(user1.getUsername(), String.valueOf(product1.getId()));
        assertNull(besketItem);
        
        basket = store.getBasketItems(user1.getUsername());
        assertEquals(0, basket.size());
        
        store.addItemToBasket(user1.getUsername(), String.valueOf(product1.getId()));
        besketItem = store.getBasketItem(user1.getUsername(), String.valueOf(product1.getId()));
        assertEquals(1, besketItem.getQuantity());
        
        store.removeItemsFromBasket(user1.getUsername());
        basket = store.getBasketItems(user1.getUsername());
        assertEquals(0, basket.size());
        besketItem = store.getBasketItem(user1.getUsername(), String.valueOf(product1.getId()));
        assertNull(besketItem);
    }
    @Ignore
    @Test
    public void updateBascketTest() {
        /*assertTrue(store.addNewUser(user1));
        store.addNewProduct(product1);
        List<Product> products = new LinkedList<Product>();
        products.add(product1);
        products.add(product1);*/
        
        //BasketKey basketKey = new BasketKey();
        //basketKey.setItem("" + product1.getId());
        //basketKey.setUser(user1.getUser());
        
        BasketItem basketItem = new BasketItem();
        basketItem.setQuantity(2);
        //basketItem.setItemId("" + product1.getId());
        basketItem.setUsername(user1.getUsername());
        
        //store.addItemsToBasket(user1.getUser(), basketItem);
        
        /*User user = store.getUser(user1.getUser());
        
        assertEquals("Should have one item in the basket", user.getBasket().size(), 2);
        
        Product product = user.getBasket().get(0);
        
        store.cleanBasket(user1.getUser());
        user = store.getUser(user1.getUser());
        
        assertEquals("Should have one item in the basket", user.getBasket().size(), 0);
        assertTrue(store.removeProduct(String.valueOf(product.getId())));
        assertTrue(store.removeUser(user1.getUser()));*/
    }
    
    @Test
    public void addNewProductTest() {
        List<Product> products1 = store.getProducts();
        
        store.addNewProduct(product1);
        
        List<Product> products2 = store.getProducts();
        
        assertEquals("should have one item more", products1.size() + 1, products2.size());
        
        for(Product product : products1) {
            products2.remove(product);
        }
        
        assertEquals("should have only one item", products2.size(), 1);
        assertEquals("should have only one item", products2.get(0).getName(), product1.getName());
        assertEquals("should have only one item", products2.get(0).getDescription(), product1.getDescription());
        assertEquals("should have only one item", products2.get(0).getPrice(), product1.getPrice(), 0.01);
        assertEquals("should have only one item", products2.get(0).getQuantity(), product1.getQuantity());
        
        product1 = products2.get(0);
        assertTrue(store.removeProduct(String.valueOf(products2.get(0).getId())));
    }
    
    @Test
    public void addNewUserTest() {
        assertTrue(store.addNewUser(user1));
        assertFalse(store.addNewUser(user1));
        assertTrue(store.removeUser(user1.getUsername()));
        assertFalse(store.removeUser(user1.getUsername()));
    }
    
    private static Product createProduct(int index) throws OsadException {
        Product product = new Product();
        product.setDescription("this is a product test " + index);
        product.setName("product test name " + index);
        product.setPrice(10.0  + index);
        product.setQuantity(3  + index);
        return product;
    }
    
    private static User createUser(int index) throws OsadException {
        User user = new User();
        user.setUsername("testusername" + index);
        user.setEmail("test@test.com");
        user.setIsAdmin(false);
        user.setPassword("testpassword");
        
        CreditCard creditCard = new CreditCard();
        creditCard.setCardtype("Visa");
        creditCard.setCardnumber("2344456745676789");
        creditCard.setCsc(132);
        creditCard.setExpire_month(10);
        creditCard.setExpire_year(2020);
        
        user.setCreditcard(creditCard);
        
        Address address = new Address();
        address.setCity("city-test");
        address.setCountry("country-test");
        address.setPostcode("postcode-test");
        address.setStreet("street-test");
        
        user.setAddress(address);
        return user;
    }
}
