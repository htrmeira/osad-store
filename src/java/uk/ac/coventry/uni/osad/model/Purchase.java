
package uk.ac.coventry.uni.osad.model;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 *
 * @author heitor
 */
@Entity
@Table(name="purchases")
public class Purchase implements Serializable {
    @Id @GeneratedValue
    private long id;
    @OneToOne
    private User user;
    private int quantity;
    @Temporal(TemporalType.DATE)
    private Date purchase_time;
    @OneToOne
    private Product product;
    
    public Purchase() {
        // required for hibernate
    }
    
    public Purchase(User user, BasketItem basketItem) {
        this.user = user;
        this.quantity = basketItem.getQuantity();
        this.product = basketItem.getProduct();
        purchase_time = new Date();
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the username
     */
    public User getUser() {
        return user;
    }

    /**
     * @param user the username to set
     */
    public void setUser(User  user) {
        this.user = user;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * @return the timestamp
     */
    public Date getTimestamp() {
        return purchase_time;
    }

    /**
     * @param timestamp the timestamp to set
     */
    public void setTimestamp(Date purchase_time) {
        this.purchase_time = purchase_time;
    }
    
    public double totalPrice() {
        return (quantity * product.getPrice());
    }
}
