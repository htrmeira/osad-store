/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.coventry.uni.osad.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

/**
 *
 * @author heitor
 */
@Entity
@Table(name = "basket")
public class BasketItem implements Serializable {

    @Id
    @GeneratedValue
    private long id;
    private String username;
    private int quantity;
    @OneToOne
    private Product product;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) {
        this.username = username;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    /**
     * @return the product
     */
    public Product getProduct() {
        return product;
    }

    /**
     * @param product the product to set
     */
    public void setProduct(Product product) {
        this.product = product;
    }

    /**
     * ****** END OF BEANS *******
     */
    public double totalPrice() {
        return (quantity * product.getPrice());
    }

    public void incrementQuantity() {
        if (quantity < product.getQuantity()) {
            quantity++;
        }
    }

    public void decrementQuantity() {
        quantity--;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null || !(obj instanceof BasketItem)) {
            return false;
        }
        BasketItem item = (BasketItem) obj;
        if (item == this) {
            return true;
        }
        return (item.getProduct().equals(this.getProduct())
                && item.getUsername().equals(this.getUsername()));
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 17 * hash + (this.username != null ? this.username.hashCode() : 0);
        hash = 17 * hash + (this.product != null ? this.product.hashCode() : 0);
        return hash;
    }

    @Override
    public String toString() {
        return String.format("NewBasketItem [id=%s, username=%s, quantity=%s, %s]",
                id, username, quantity, product);
    }
}
