package uk.ac.coventry.uni.osad.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import uk.ac.coventry.uni.osad.util.OsadException;
import static uk.ac.coventry.uni.osad.util.StringUtils.checkCreditCardANumber;
import static uk.ac.coventry.uni.osad.util.StringUtils.checkNullOrEmpty;

/**
 *
 * @author heitor
 */
@Entity
@Table(name="creditcards")
public class CreditCard implements Serializable {
    @Id @GeneratedValue
    private long id;
    private String cardtype;
    private String cardnumber;
    private int expire_month;
    private int expire_year;
    private int csc;
    
    public CreditCard() {}
    
    public CreditCard(String cardtype, String card_number, int expire_month, 
            int expire_year, int csc) throws OsadException {
        setCardtype(cardtype);
        setCardnumber(card_number);
        setExpire_month(expire_month);
        setExpire_year(expire_year);
        setCsc(csc);
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the cardnumber
     */
    public String getCardnumber() {
        return cardnumber;
    }

    /**
     * @param cardnumber the card_number to set
     */
    public void setCardnumber(String cardnumber) throws OsadException {
        checkNullOrEmpty("This card number is invalid", cardnumber);
        checkCreditCardANumber("This card number is invalid", cardnumber);
        this.cardnumber = cardnumber;
    }

    /**
     * @return the expire_month
     */
    public int getExpire_month() {
        return expire_month;
    }

    /**
     * @param expire_month the expire_month to set
     */
    public void setExpire_month(int expire_month) {
        this.expire_month = expire_month;
    }

    /**
     * @return the expire_year
     */
    public int getExpire_year() {
        return expire_year;
    }

    /**
     * @param expire_year the expire_year to set
     */
    public void setExpire_year(int expire_year) {
        this.expire_year = expire_year;
    }

    /**
     * @return the csc
     */
    public int getCsc() {
        return csc;
    }

    /**
     * @param csc the csc to set
     */
    public void setCsc(int csc) {
        this.csc = csc;
    }

    /**
     * @return the cardtype
     */
    public String getCardtype() {
        return cardtype;
    }

    /**
     * @param cardtype the cardtype to set
     */
    public void setCardtype(String cardtype) throws OsadException {
        checkNullOrEmpty("Credit card type can not be empty", cardtype);
        this.cardtype = cardtype;
    }
}
