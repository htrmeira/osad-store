package uk.ac.coventry.uni.osad.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import uk.ac.coventry.uni.osad.util.OsadException;
import static uk.ac.coventry.uni.osad.util.StringUtils.checkNullOrEmpty;

/**
 *
 * @author heitor
 */
@Entity
@Table(name="addresses")
public class Address implements Serializable {
    @Id @GeneratedValue
    private long id;
    private String street;
    private String postcode;
    private String city;
    private String country;
    
    public Address() {}
    
    public Address(String street, String postcode, String city, String country) throws OsadException {
        setStreet(street);
        setPostcode(postcode);
        setCity(city);
        setCountry(country);
    }

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return the street
     */
    public String getStreet() {
        return street;
    }

    /**
     * @param street the street to set
     */
    public void setStreet(String street) throws OsadException {
        checkNullOrEmpty("Street can not be empty", street);
        this.street = street;
    }

    /**
     * @return the postcode
     */
    public String getPostcode() {
        return postcode;
    }

    /**
     * @param postcode the postcode to set
     */
    public void setPostcode(String postcode) throws OsadException {
        checkNullOrEmpty("Postcode can not be empty", postcode);
        this.postcode = postcode;
    }

    /**
     * @return the city
     */
    public String getCity() {
        return city;
    }

    /**
     * @param city the city to set
     */
    public void setCity(String city) throws OsadException {
        checkNullOrEmpty("City can not be empty", city);
        this.city = city;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @param country the country to set
     */
    public void setCountry(String country) throws OsadException {
        checkNullOrEmpty("Country can not be empty", city);
        this.country = country;
    }
    
}
