/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.coventry.uni.osad.model;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;
import uk.ac.coventry.uni.osad.util.OsadException;
import uk.ac.coventry.uni.osad.util.StringUtils;

/**
 *
 * @author heitor
 */
@Entity
@Table(name="products")
public class Product implements Serializable {
    
    @Id @GeneratedValue
    private long id;
    private int quantity;
    private String name;
    private String description;
    private double price;

    /**
     * @return the id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id the id to set
     */
    public void setId(long Id) {
        this.id = Id;
    }

    /**
     * @return the name
     */
    public String getName() {
        return name;
    }

    /**
     * @param name the name to set
     */
    public void setName(String name) throws OsadException {
        StringUtils.checkNullOrEmpty("Product name can not be empty", name);
        this.name = name;
    }

    /**
     * @return the description
     */
    public String getDescription() {
        return description;
    }

    /**
     * @param description the description to set
     */
    public void setDescription(String description) throws OsadException {
        StringUtils.checkNullOrEmpty("Product description can not be empty", description);
        this.description = description;
    }

    /**
     * @return the price
     */
    public double getPrice() {
        return price;
    }

    /**
     * @param price the price to set
     */
    public void setPrice(double price) throws OsadException {
        if(price < 0) {
            throw new OsadException("Product price must be a positive number");
        }
        this.price = price;
    }

    /**
     * @return the quantity
     */
    public int getQuantity() {
        return quantity;
    }

    /**
     * @param quantity the quantity to set
     */
    public void setQuantity(int quantity) throws OsadException {
        if(quantity < 0) {
            throw new OsadException("Product quantity must be a positive number");
        }
        this.quantity = quantity;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Product && obj != null) {
            Product otherProduct = (Product) obj;
            return otherProduct.getId() == getId();
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 5;
        hash = 29 * hash + (int) (this.id ^ (this.id >>> 32));
        return hash;
    }    
    
    @Override
    public String toString() {
        return String.format("Product [id=%s, quantity=%s, name=%s, description=%s, "
                + "price=%s]", getId(), getQuantity(), getName(), getDescription()
                , getPrice());
    }
}
