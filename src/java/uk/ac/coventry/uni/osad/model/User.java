
package uk.ac.coventry.uni.osad.model;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import uk.ac.coventry.uni.osad.util.OsadException;
import static uk.ac.coventry.uni.osad.util.StringUtils.checkEmail;
import static uk.ac.coventry.uni.osad.util.StringUtils.checkNull;
import static uk.ac.coventry.uni.osad.util.StringUtils.checkNullOrEmpty;

/**
 *
 * @author Heitor Meira - demeloh@uni.coventry.ac.uk
 */
@Entity
@Table(name="users")
public class User implements Serializable {

    @Id
    private String username;
    private String email;
    private String password;
    private boolean isAdmin;
    @OneToOne(fetch= FetchType.EAGER, cascade= CascadeType.ALL)
    private CreditCard creditcard;
    @OneToOne(fetch= FetchType.EAGER, cascade= CascadeType.ALL)
    private Address address;
    //@ManyToMany(cascade=CascadeType.ALL, fetch= FetchType.EAGER)
    //private List<Product> basket = new LinkedList<Product>();
    
    public User() {}
    
    public User(String username, String email, String password, boolean isAdmin, 
            CreditCard creditcard, Address address) throws OsadException {
        setUsername(username);
        setEmail(email);
        setPassword(password);
        setIsAdmin(isAdmin);
        setCreditcard(creditcard);
        setAddress(address);
    }

    /**
     * @return the username
     */
    public String getUsername() {
        return username;
    }

    /**
     * @param username the username to set
     */
    public void setUsername(String username) throws OsadException {
        checkNullOrEmpty("Username can not be empty", username);
        this.username = username;
    }

    /**
     * @return the password
     */
    public String getPassword() {
        return password;
    }

    /**
     * @param password the password to set
     */
    public void setPassword(String password) throws OsadException {
        checkNullOrEmpty("Password can not be empty", password);
        this.password = password;
    }

    /**
     * @return the isAdmin
     */
    public boolean isIsAdmin() {
        return isAdmin;
    }

    /**
     * @param isAdmin the isAdmin to set
     */
    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }
    
    @Override
    public boolean equals(Object obj) {
        if(obj instanceof User && obj != null) {
            User otherProduct = (User) obj;
            return otherProduct.getUsername().equals(getUsername());
        }
        return false;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 41 * hash + (this.username != null ? this.username.hashCode() : 0);
        return hash;
    }
    
    @Override
    public String toString() {
        return String.format("User [username=%s, password=%s]", username, password);
    }

    /**
     * @return the credit_card
     */
    public CreditCard getCreditcard() {
        return creditcard;
    }

    /**
     * @param creditcard the credit_card to set
     */
    public void setCreditcard(CreditCard creditcard) throws OsadException {
        checkNull("Credit card can not be null", creditcard);
        this.creditcard = creditcard;
    }

    /**
     * @return the email
     */
    public String getEmail() {
        return email;
    }

    /**
     * @param email the email to set
     */
    public void setEmail(String email) throws OsadException {
        checkNullOrEmpty("Email can not be empty", email);
        checkEmail("This email is not valid", email);
        this.email = email;
    }

    /**
     * @return the address
     */
    public Address getAddress() {
        return address;
    }

    /**
     * @param address the address to set
     */
    public void setAddress(Address address) throws OsadException {
        checkNull("Credit card can not be null", address);
        this.address = address;
    }
}
