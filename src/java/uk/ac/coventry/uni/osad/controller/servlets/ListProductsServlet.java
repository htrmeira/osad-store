/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.coventry.uni.osad.controller.servlets;

import java.io.IOException;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uk.ac.coventry.uni.osad.controller.store.SingletonStore;
import uk.ac.coventry.uni.osad.model.Address;
import uk.ac.coventry.uni.osad.model.CreditCard;
import uk.ac.coventry.uni.osad.model.Product;
import uk.ac.coventry.uni.osad.model.User;
import uk.ac.coventry.uni.osad.util.OsadException;

/**
 *
 * @author heitor
 */
public class ListProductsServlet extends HttpServlet {
    private static boolean adminExists = false;

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
            
        SingletonStore store = SingletonStore.instance();
        
        if(!adminExists) {
            store.addNewUser(addUserAdmin());
            adminExists = true;
        }
            
        List<Product> products = removeUnavailableProducts(store.getProducts());
        //List<Product> products = store.getProducts();
        
        request.setAttribute("products", products);
        
        String jsp_header = "product/list_products.jsp";
        if(isLogged(request)) {
            jsp_header = "product/list_products_purchasable.jsp";
        }
        
        RequestDispatcher dispatcher = request.getRequestDispatcher(jsp_header);
        if (dispatcher != null){
            dispatcher.include(request, response);
        }
    }
    
    private List<Product> removeUnavailableProducts(List<Product> products) {
        if(products == null) {
            return new LinkedList<Product>();
        }
        List<Product> resultProducts = new LinkedList<Product>();
        for(Product product : products) {
            if(product.getQuantity() > 0) {
                resultProducts.add(product);
            }
        }
        return resultProducts;
    }
    
    private boolean isLogged(HttpServletRequest request) {
        return request.getSession().getAttribute("logged_username") != null;
    }
    
    private User addUserAdmin() {
        try {
            CreditCard creditCard = new CreditCard("Visa", "0000000000000000",
                    10, 2020, 110);
            Address address = new Address("admin", "admin", "admin", "admin");
            User user = new User("admin", "admin@admin.com", "admin", true, 
                    creditCard, address);
            return user;
        } catch (OsadException ex) {
            Logger.getLogger(ListProductsServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    private User getLoggedUser(HttpServletRequest request) {
        return (User) request.getSession().getAttribute("logged_username");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
