/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.coventry.uni.osad.controller.servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uk.ac.coventry.uni.osad.controller.store.SingletonStore;
import uk.ac.coventry.uni.osad.model.Address;
import uk.ac.coventry.uni.osad.model.CreditCard;
import uk.ac.coventry.uni.osad.model.User;
import uk.ac.coventry.uni.osad.util.OsadException;
import uk.ac.coventry.uni.osad.util.StringUtils;

/**
 *
 * @author heitor
 */
public class AdminUserServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            SingletonStore store = SingletonStore.instance();
            executeAction(request, store);
            response.sendRedirect("/osad-store/admin/manage_users.jsp");
        } catch(Exception ex) {
            request.setAttribute("error", ex.getMessage());
            RequestDispatcher dispatcher = request.getRequestDispatcher("/visualize_user?user_to_visualize=" + getUsername(request));
            if (dispatcher != null){
                dispatcher.forward(request, response);
            }
        }
    }
    
    private void executeAction(HttpServletRequest request, SingletonStore store) throws OsadException {
        String username = getUsername(request);
        User user = buildUser(request);   
        if (username == null) {
            return;
        }
        if (user == null) {
            return;
        }

        if (isModifyAction(request)) {
            store.updateUser(user);
        } else if (isRemoveAction(request)) {
            store.removeUser(username);
        }
    }
    
    private boolean isRemoveAction(HttpServletRequest request) {
        return request.getParameter("remove_button") != null;
    }

    private boolean isModifyAction(HttpServletRequest request) {
        return request.getParameter("modify_button") != null;
    }
    
    private User buildUser(HttpServletRequest request) throws OsadException {
        return new User(getUsername(request), getEmail(request), getPassword(request), 
                asAdmin(request), getCreditCard(request), getAddress(request));
    }
    
    private Address getAddress(HttpServletRequest request) throws OsadException {
        return new Address(getStreet(request), getPostcode(request), 
                getCity(request), getCountry(request));
    }
    
    private CreditCard getCreditCard(HttpServletRequest request) throws OsadException {
        return new CreditCard(getCreditCardType(request), getCardNumber(request), getExpireMonth(request), 
                getExpireYear(request), getCSCNumber(request));
    }
    
    /********* begin of User information *********/
    private String getUsername(HttpServletRequest request) {
        return request.getParameter("username");
    }
    
    private String getPassword(HttpServletRequest request) {
        return request.getParameter("password");
    }
    
    private boolean asAdmin(HttpServletRequest request) {
        return request.getParameter("admin_checkbox") != null;
    }
    
    private String getEmail(HttpServletRequest request) {
        return request.getParameter("email");
    }
    /********* end of User information *********/
    
    /********* begin of CreditCard information *********/
    private String getCreditCardType(HttpServletRequest request) {
        return request.getParameter("cards");
    }
    
    private String getCardNumber(HttpServletRequest request) {
        return request.getParameter("card_number");
    }
    
    private int getExpireMonth(HttpServletRequest request) throws OsadException {
        String expireMonth = (String) request.getParameter("expire_month");
        StringUtils.checkNullOrEmpty("Expire month can not be empty", expireMonth);
        StringUtils.checkIsANumber("Expire month must have only numbers", expireMonth);
        return Integer.parseInt(expireMonth);
    }
    
    private int getExpireYear(HttpServletRequest request) throws OsadException {
        String expireYear = (String) request.getParameter("expire_month");
        StringUtils.checkNullOrEmpty("Expire year can not be empty", expireYear);
        StringUtils.checkIsANumber("Expire year must have only numbers", expireYear);
        return Integer.parseInt(expireYear);
    }
    
    private int getCSCNumber(HttpServletRequest request) throws OsadException {
        String cscNumber = (String) request.getParameter("csc_number");
        StringUtils.checkNullOrEmpty("CSC can not be empty", cscNumber);
        StringUtils.checkIsANumber("CSC must have only numbers", cscNumber);
        return Integer.parseInt(cscNumber);
    }
    /********* end of CreditCard information *********/
    
    /********* begin of Address information *********/
    private String getStreet(HttpServletRequest request) {
        return request.getParameter("street");
    }
    
    private String getPostcode(HttpServletRequest request) {
        return request.getParameter("postcode");
    }
    
    private String getCity(HttpServletRequest request) {
        return request.getParameter("city");
    }
    
    private String getCountry(HttpServletRequest request) {
        return request.getParameter("country");
    }
    /********* end of Address information *********/

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
