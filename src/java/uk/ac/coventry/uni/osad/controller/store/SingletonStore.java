/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.coventry.uni.osad.controller.store;

import java.util.LinkedList;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.Transaction;
import uk.ac.coventry.uni.osad.model.BasketItem;
import uk.ac.coventry.uni.osad.model.Product;
import uk.ac.coventry.uni.osad.model.Purchase;
import uk.ac.coventry.uni.osad.model.User;
import uk.ac.coventry.uni.osad.util.DatabaseUtil;
import uk.ac.coventry.uni.osad.util.OsadException;

/**
 *
 * @author heitor
 */
public class SingletonStore {
    private final static String PRODUCTS_TABLE = "Product";
    private final static String USERS_TABLE = "User";
    private final static String BASKET_TABLE = "BasketItem";
    private final static String PURCHASE_TABLE = "Purchase";
    
    private final static SingletonStore thisInstance = new SingletonStore();
    
    private SingletonStore() { }
    
    /***************** BEGIN OF PRODUCTS *****************/
    
    public void addNewProduct(Product product) {
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            session.saveOrUpdate(product);
            
            transaction.commit();
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            System.err.println(ex.getStackTrace());
        } finally {
            session.close();
        }
    }
    
    public boolean removeProduct(String productId) {
        boolean status = true;
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            Query query = session.createQuery("delete from " + PRODUCTS_TABLE + " where id='" + productId + "'");
            status = query.executeUpdate() > 0;
            
            transaction.commit();
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            status = false;;
        } finally {
            session.close();
        }
        return status;
    }
    
    public List<Product> getProducts() {
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            List<Product> products = session.createQuery("from " + PRODUCTS_TABLE).list();
            
            transaction.commit();
            return products;
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }
    
    public Product getProduct(String productId) {
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            Query query = session.createQuery("from " + PRODUCTS_TABLE + " where id='" + productId + "'");
            
            Product product = (Product)query.uniqueResult();
            transaction.commit();
            return product;
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }
    
    /***************** END OF PRODUCTS *****************/
    
    /***************** BEGIN OF USER *****************/
    
    public boolean login(User user) {
        User storedUser = getUser(user.getUsername());
        
        if(storedUser == null) {
            return false;
        } else if(!storedUser.getPassword().equals(user.getPassword())) {
            return false;
        } else {
            if(user.isIsAdmin()) {
                return storedUser.isIsAdmin();
            } else {
                return true;
            }
        }
    }
    
    public boolean addNewUser(User user) {
        boolean status = true;
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            if(loginExists(user.getUsername())) {
                status = false;
            } else {
                session.saveOrUpdate(user);
                status = true;
            }
            
            transaction.commit();
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            status = false;
        } finally {
            session.close();
        }
        return status;
    }
    
    public boolean updateUser(User user) {
        boolean status = true;
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();

            session.saveOrUpdate(user);
            status = true;

            transaction.commit();
        } catch (Exception ex) {
            if (transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            status = false;
        } finally {
            session.close();
        }
        return status;
    }
    
    public boolean removeUser(String username) {
        boolean status = true;
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            Query query = session.createQuery("delete from " + USERS_TABLE + " where username='" + username + "'");
            status = query.executeUpdate() > 0;
            
            transaction.commit();
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            status = false;;
        } finally {
            session.close();
        }
        return status;
    }
    
    public List<User> getUsers() {
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            List<User> users = session.createQuery("from " + USERS_TABLE).list();
            
            transaction.commit();
            return users;
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }
    
    public User getUser(String username) {
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            Query query = session.createQuery("from " + USERS_TABLE + " where username='" + username + "'");

            User product = (User) query.uniqueResult();
            transaction.commit();
            return product;
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }
    
    private boolean loginExists(String username) {
        return getUser(username) != null;
    }
    
    /***************** END OF USER *****************/
    
    
    public static final SingletonStore instance() {
        return thisInstance;
    }
    
    /***************** BEGIN OF BASKET *****************/
    
    public List<BasketItem> getBasketItems(String username) {
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            List<BasketItem> basketItems = session.createQuery("from " + BASKET_TABLE + 
                    " where username='" + username + "'").list();
            
            transaction.commit();
            return basketItems;
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            System.err.println(ex.getStackTrace());
            return new LinkedList<BasketItem>();
        } finally {
            session.close();
        }
    }
    
    public BasketItem getBasketItem(String username, String productId) {
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            Query query = session.createQuery("from " + BASKET_TABLE + 
                    " where username='" + username + "' and product_id='" + productId + "'");
            
            BasketItem newBasketItem = (BasketItem) query.uniqueResult();
            transaction.commit();
            return newBasketItem;
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }
    
    public BasketItem getBasketItem(String itemId) {
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            Query query = session.createQuery("from " + BASKET_TABLE + 
                    " where id='" + itemId + "'");
            
            BasketItem newBasketItem = (BasketItem) query.uniqueResult();
            transaction.commit();
            return newBasketItem;
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            return null;
        } finally {
            session.close();
        }
    }
    
    public void addItemToBasket(String username, String productId) throws OsadException {
        BasketItem newItemBasket = getBasketItemOrCreateStub(username, productId);
        System.out.println("newItemBasket = " + newItemBasket);
        if(newItemBasket.getProduct().getQuantity() <= 0) {
            return;
        }
                
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            session.saveOrUpdate(newItemBasket);
            
            transaction.commit();
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            System.err.println(ex.getStackTrace());
        } finally {
            session.close();
        }
    }
    
    public boolean removeItemFromBasket(String username, String productId) {
        boolean status = true;
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            Query query = session.createQuery("delete from " + BASKET_TABLE + 
                    " where username='" + username + "' and product_id='" + productId + "'");
            status = query.executeUpdate() > 0;
            
            transaction.commit();
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            status = false;
        } finally {
            session.close();
        }
        return status;
    }
    
    public boolean removeItemFromBasket(String itemId) {
        boolean status = true;
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            Query query = session.createQuery("delete from " + BASKET_TABLE + 
                    " where id='" + itemId + "'");
            status = query.executeUpdate() > 0;
            
            transaction.commit();
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            status = false;
        } finally {
            session.close();
        }
        return status;
    }
    
    public boolean removeItemsFromBasket(String username) {
        boolean status = true;
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            Query query = session.createQuery("delete from " + BASKET_TABLE + 
                    " where username='" + username + "'");
            status = query.executeUpdate() > 0;
            
            transaction.commit();
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            status = false;
        } finally {
            session.close();
        }
        return status;
    }
    
    public void updateBasketItem(BasketItem basketItem) {
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            session.saveOrUpdate(basketItem);
            
            transaction.commit();
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            System.err.println(ex.getStackTrace());
        } finally {
            session.close();
        }
    }
    
    private BasketItem getBasketItemOrCreateStub(String username, String productId) throws OsadException {
        BasketItem newItemBasket = getBasketItem(username,  productId);
        
        if(newItemBasket == null) {
            newItemBasket = new BasketItem();
            newItemBasket.setUsername(username);
            newItemBasket.setQuantity(1);
            Product product = new Product();
            product.setId(Long.parseLong(productId));
            product.setQuantity(1);
            newItemBasket.setProduct(product);
        } else {
            newItemBasket.incrementQuantity();
        }
        return newItemBasket;
    }
    
    /***************** END OF BASKET *****************/
    
    /***************** BEGIN OF PURCHASE *****************/
    
    public void updateProduct(Product product) {
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            if(product.getQuantity() <= 0) {
                session.saveOrUpdate(product);
                removeProduct(""+product.getId());
            } else {
                session.saveOrUpdate(product);
            }
            
            transaction.commit();
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            System.err.println(ex.getStackTrace());
        } finally {
            session.close();
        }
    }
    
    private void addBasketItemToPurchase(BasketItem basketItem) {
        User user = getUser(basketItem.getUsername());
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            decreaseFromProduct(basketItem);
            updateProduct(basketItem.getProduct());
            session.saveOrUpdate(new Purchase(user, basketItem));
            
            transaction.commit();
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            System.err.println(ex.getStackTrace());
        } finally {
            session.close();
        }
    }
    
    private void decreaseFromProduct(BasketItem basketItem) throws OsadException {
        int quantity = basketItem.getQuantity();
        if(basketItem.getProduct().getQuantity() - quantity >= 0) {
         basketItem.getProduct().setQuantity(basketItem.getProduct().getQuantity() - quantity);   
        } else {
            basketItem.getProduct().setQuantity(basketItem.getProduct().getQuantity());
            basketItem.getProduct().setQuantity(0);
        }
    }
    
    public void executePurchase(String username) {
        System.out.println("executing purchase ");
        
        List<BasketItem> items = getBasketItems(username);
        for (BasketItem basketItem : items) {
            addBasketItemToPurchase(basketItem);
        }
        
        removeItemsFromBasket(username);
    }
    
    public boolean removePurchases(String username) {
        boolean status = true;
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            Query query = session.createQuery("delete from " + PURCHASE_TABLE + 
                    " where user_username='" + username + "'");
            status = query.executeUpdate() > 0;
            
            transaction.commit();
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            status = false;
        } finally {
            session.close();
        }
        return status;
    }
    
    public boolean removePurchase(String purchaseId) {
        boolean status = true;
        Purchase purchase = getPurchase(purchaseId);
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            
            Query query = session.createQuery("delete from " + PURCHASE_TABLE + 
                    " where id='" + purchaseId + "'");
            status = query.executeUpdate() > 0;
            
            transaction.commit();
            updateProduct(purchase.getProduct());
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            ex.printStackTrace();
            status = false;
        } finally {
            session.close();
        }
        return status;
    }
    
    public List<Purchase> getPurchases(String username) {
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            List<Purchase> purchasedItems = session.createQuery("from " + PURCHASE_TABLE + 
                    " where user_username='" + username + "'").list();
            
            transaction.commit();
            return purchasedItems;
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            System.err.println(ex.getStackTrace());
            return new LinkedList<Purchase>();
        } finally {
            session.close();
        }
    }
    
    public Purchase getPurchase(String purchaseId) {
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            Query query = session.createQuery("from " + PURCHASE_TABLE + 
                    " where id='" + purchaseId + "'");
            Purchase purchase = (Purchase) query.uniqueResult();
            transaction.commit();
            return purchase;
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            System.err.println(ex.getStackTrace());
            return null;
        } finally {
            session.close();
        }
    }
    
    public List<Purchase> getPurchases() {
        Session session = DatabaseUtil.getSession();
        Transaction transaction = null;
        try {
            transaction = session.getTransaction();
            transaction.begin();
            
            List<Purchase> purchases = session.createQuery("from " + PURCHASE_TABLE).list();
            
            transaction.commit();
            return purchases;
        } catch(Exception ex) {
            if(transaction != null) {
                transaction.rollback();
            }
            System.err.println(ex.getStackTrace());
            return null;
        } finally {
            session.close();
        }
    }
    /***************** END OF PURCHASE *****************/
}
