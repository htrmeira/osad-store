/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.coventry.uni.osad.controller.servlets;

import java.io.IOException;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uk.ac.coventry.uni.osad.controller.store.SingletonStore;
import uk.ac.coventry.uni.osad.model.Product;
import uk.ac.coventry.uni.osad.util.OsadException;
import uk.ac.coventry.uni.osad.util.StringUtils;

/**
 *
 * @author heitor
 */
public class AddProductServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            Product product = buildProduct(request);
            SingletonStore store = SingletonStore.instance();
            store.addNewProduct(product);
            response.sendRedirect("index.jsp");
        } catch(Exception ex) {
            System.out.println("[Servlet] error: " + ex.getMessage());
            copyParameterToAttributes(request);
            request.setAttribute("error", ex.getMessage());
            RequestDispatcher dispatcher = request.getRequestDispatcher("/admin/add_product.jsp");
            if (dispatcher != null){
                dispatcher.forward(request, response);
            }
        }
    }
    
    private void copyParameterToAttributes(HttpServletRequest request) {
        Enumeration<String> enumParameters = request.getParameterNames();
        for(String parameter = ""; enumParameters.hasMoreElements(); parameter = enumParameters.nextElement()) {
            String parameterValue = request.getParameter(parameter);
            if(parameter != null && parameterValue != null) {
                request.setAttribute(parameter, parameterValue);
            }
        }
    }
    
    private Product buildProduct(HttpServletRequest request) throws OsadException {
        Product product = new Product();
        product.setDescription(request.getParameter("description"));
        product.setName(request.getParameter("name"));
        product.setPrice(getPrice(request));
        product.setQuantity(getQuantity(request));
        return product;
    }
    
    private int getQuantity(HttpServletRequest request) throws OsadException {
        String quantity = (String) request.getParameter("quantity");
        StringUtils.checkNullOrEmpty("Product quantity can not be empty", quantity);
        StringUtils.checkIsADouble("Product quantity must be a number", quantity);
        return Integer.parseInt(quantity);
    }
    
    private double getPrice(HttpServletRequest request) throws OsadException {
        String price = (String) request.getParameter("price");
        StringUtils.checkNullOrEmpty("Product price can not be empty", price);
        StringUtils.checkIsADouble("Product price must be a number", price);
        return Double.valueOf(price);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
