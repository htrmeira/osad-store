/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.coventry.uni.osad.controller.servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uk.ac.coventry.uni.osad.model.User;

/**
 *
 * @author heitor
 */
@WebServlet(name = "IndexHeaderServlet", urlPatterns = {"/index_header"})
public class IndexHeaderServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        String jsp_header = "/headers/not_logged_header.jsp";
        if(isLogged(request)) {
            if(getLoggedUser(request).isIsAdmin()) {
                jsp_header = "/headers/admin_header.jsp";
            } else {
                jsp_header = "/headers/logged_header.jsp";
            }
        }
        
        RequestDispatcher dispatcher = request.getRequestDispatcher(jsp_header);
        if (dispatcher != null){
            dispatcher.include(request, response);
        }
    }
    
    private boolean isLogged(HttpServletRequest request) {
        return request.getSession().getAttribute("logged_username") != null;
    }
    
    private User getLoggedUser(HttpServletRequest request) {
        return (User) request.getSession().getAttribute("logged_username");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
