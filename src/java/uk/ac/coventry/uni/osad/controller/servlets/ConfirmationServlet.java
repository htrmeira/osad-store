/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.coventry.uni.osad.controller.servlets;

import java.io.IOException;
import java.util.List;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uk.ac.coventry.uni.osad.controller.store.SingletonStore;
import uk.ac.coventry.uni.osad.model.BasketItem;
import uk.ac.coventry.uni.osad.model.User;

/**
 *
 * @author heitor
 */
public class ConfirmationServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        if (isLogged(request)) {
            SingletonStore store = SingletonStore.instance();
            User user = getLoggedUser(request);
            List<BasketItem> items = store.getBasketItems(user.getUsername());
            System.out.println("items: " + items);
            request.setAttribute("items", items);
            request.setAttribute("total_price", setTotalPrice(items));   
            request.setAttribute("total_quantity", setTotalQuantity(items));
        }
        
        RequestDispatcher dispatcher = request.getRequestDispatcher("/confirm.jsp");
        if (dispatcher != null){
            //dispatcher.include(request, response);
            dispatcher.forward(request, response);
        }

        //response.sendRedirect("confirm.jsp");
    }
    
    private int setTotalQuantity(List<BasketItem> basket) {
        int totalQuantity = 0;
        for(BasketItem basketItem : basket) {
            totalQuantity += basketItem.getQuantity();
        }
        return totalQuantity;
    }
    
    private double setTotalPrice(List<BasketItem> basket) {
        double totalPrice = 0;
        for(BasketItem basketItem : basket) {
            totalPrice += basketItem.totalPrice();
        }
        return totalPrice;
    }
    
    private boolean isLogged(HttpServletRequest request) {
        return request.getSession().getAttribute("logged_username") != null;
    }
    
    private User getLoggedUser(HttpServletRequest request) {
        return (User) request.getSession().getAttribute("logged_username");
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
 