package uk.ac.coventry.uni.osad.controller.servlets;

import java.io.IOException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uk.ac.coventry.uni.osad.controller.store.SingletonStore;
import uk.ac.coventry.uni.osad.model.User;
import uk.ac.coventry.uni.osad.util.OsadException;

/**
 *
 * @author heitor
 */
public class LoginServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            SingletonStore store = SingletonStore.instance();
            User user = new User();
            user.setIsAdmin(asAdmin(request));
            user.setPassword(getPassword(request));
            user.setUsername(getUsername(request));
            if (store.login(user)) {
                sessionLoggin(request, store.getUser(user.getUsername()));
                System.out.println("login accpeted");
                response.sendRedirect("index.jsp");
            } else {
                throw new OsadException("Invalid login or password");
                //response.sendRedirect("/user/login.jsp");
            }
        } catch (Exception ex) {
            System.out.println("[Servlet] error: " + ex.getMessage());
            request.setAttribute("error", ex.getMessage());
            RequestDispatcher dispatcher = request.getRequestDispatcher("/user/login.jsp");
            if (dispatcher != null) {
                dispatcher.forward(request, response);
            }
        }
    }

    private void sessionLoggin(HttpServletRequest request, User user) {
        request.getSession().setAttribute("logged_username", user);
    }

    private String getUsername(HttpServletRequest request) {
        return request.getParameter("username");
    }

    private String getPassword(HttpServletRequest request) {
        return request.getParameter("password");
    }

    private boolean asAdmin(HttpServletRequest request) {
        return request.getParameter("admin_checkbox") != null;
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
