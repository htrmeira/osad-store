/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.coventry.uni.osad.controller.servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Enumeration;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import uk.ac.coventry.uni.osad.controller.store.SingletonStore;
import uk.ac.coventry.uni.osad.model.Address;
import uk.ac.coventry.uni.osad.model.CreditCard;
import uk.ac.coventry.uni.osad.model.User;
import uk.ac.coventry.uni.osad.util.OsadException;
import uk.ac.coventry.uni.osad.util.StringUtils;

/**
 *
 * @author heitor
 */
public class ModifyUserServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP
     * <code>GET</code> and
     * <code>POST</code> methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            SingletonStore store = SingletonStore.instance();
            User user = buildUser(request);
            store.updateUser(user);
            sessionLoggin(request, user);
            response.sendRedirect("index.jsp");
        } catch(Exception ex) {
            System.out.println("[Servlet] error: " + ex.getMessage());
            copyParameterToAttributes(request);
            request.setAttribute("error", ex.getMessage());
            RequestDispatcher dispatcher = request.getRequestDispatcher("/user/myaccount.jsp");
            if (dispatcher != null){
                dispatcher.forward(request, response);
            }
        }
    }
    
    private void sessionLoggin(HttpServletRequest request, User user) {
        request.getSession().setAttribute("logged_username", user);
    }
    
    private void copyParameterToAttributes(HttpServletRequest request) {
        Enumeration<String> enumParameters = request.getParameterNames();
        for(String parameter = ""; enumParameters.hasMoreElements(); parameter = enumParameters.nextElement()) {
            String parameterValue = request.getParameter(parameter);
            if(parameter != null && parameterValue != null) {
                request.setAttribute(parameter, parameterValue);
            }
        }
    }
    
    private User buildUser(HttpServletRequest request) throws OsadException {
        return new User(getUsername(request), getEmail(request), getPassword(request), 
                asAdmin(request), getCreditCard(request), getAddress(request));
    }
    
    private Address getAddress(HttpServletRequest request) throws OsadException {
        return new Address(getStreet(request), getPostcode(request), 
                getCity(request), getCountry(request));
    }
    
    private CreditCard getCreditCard(HttpServletRequest request) throws OsadException {
        return new CreditCard(getCreditCardType(request),getCardNumber(request), getExpireMonth(request), 
                getExpireYear(request), getCSCNumber(request));
    }
    
    /********* begin of User information *********/
    private String getCreditCardType(HttpServletRequest request) {
        return request.getParameter("cards");
    }
    
    private String getUsername(HttpServletRequest request) {
        return request.getParameter("username");
    }
    
    private String getPassword(HttpServletRequest request) {
        return request.getParameter("password");
    }
    
    private boolean asAdmin(HttpServletRequest request) {
        return request.getParameter("admin_checkbox") != null;
    }
    
    private String getEmail(HttpServletRequest request) {
        return request.getParameter("email");
    }
    /********* end of User information *********/
    
    /********* begin of CreditCard information *********/
    private String getCardNumber(HttpServletRequest request) {
        return request.getParameter("card_number");
    }
    
    private int getExpireMonth(HttpServletRequest request) throws OsadException {
        String expireMonth = (String) request.getParameter("expire_month");
        StringUtils.checkNullOrEmpty("Expire month can not be empty", expireMonth);
        StringUtils.checkIsANumber("Expire month must have only numbers", expireMonth);
        return Integer.parseInt(expireMonth);
    }
    
    private int getExpireYear(HttpServletRequest request) throws OsadException {
        String expireYear = (String) request.getParameter("expire_month");
        StringUtils.checkNullOrEmpty("Expire year can not be empty", expireYear);
        StringUtils.checkIsANumber("Expire year must have only numbers", expireYear);
        return Integer.parseInt(expireYear);
    }
    
    private int getCSCNumber(HttpServletRequest request) throws OsadException {
        String cscNumber = (String) request.getParameter("csc_number");
        StringUtils.checkNullOrEmpty("CSC can not be empty", cscNumber);
        StringUtils.checkIsANumber("CSC must have only numbers", cscNumber);
        return Integer.parseInt(cscNumber);
    }
    /********* end of CreditCard information *********/
    
    /********* begin of Address information *********/
    private String getStreet(HttpServletRequest request) {
        return request.getParameter("street");
    }
    
    private String getPostcode(HttpServletRequest request) {
        return request.getParameter("postcode");
    }
    
    private String getCity(HttpServletRequest request) {
        return request.getParameter("city");
    }
    
    private String getCountry(HttpServletRequest request) {
        return request.getParameter("country");
    }
    /********* end of Address information *********/

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP
     * <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP
     * <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>
}
