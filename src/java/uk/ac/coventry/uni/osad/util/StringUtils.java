/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.coventry.uni.osad.util;

import java.util.regex.Pattern;

/**
 *
 * @author heitor
 */
public class StringUtils {
    /*
     * Pattern provided by: http://stackoverflow.com/questions/153716/verify-email-in-java
     * Thanks MBCook (http://stackoverflow.com/users/18189/mbcook)
     */
    private static final Pattern rfc2822 = Pattern.compile("^[a-z0-9!#$%&'*+/=?"
            + "^_`{|}~-]+(?:\\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:"
            + "[a-z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?$");
    
    public static void checkNull(String message, Object obj) throws OsadException {
        if(obj == null) {
            throw new OsadException(message);
        }
    }
    
    public static void checkNullOrEmpty(String message, String obj) throws OsadException {
        checkNull(message, obj);
        if(message.trim().isEmpty()) {
            throw new OsadException(message);
        }
    }
    
    public static void checkIsANumber(String message, String obj) throws OsadException {
        try {
            Integer.parseInt(obj);
        } catch(NumberFormatException ex) {
            throw new OsadException(message);
        }
    }
    
    public static void checkIsADouble(String message, String obj) throws OsadException {
        try {
            Double.parseDouble(obj);
        } catch(NumberFormatException ex) {
            throw new OsadException(message);
        }
    }
    
    public static void checkEmail(String message, String obj) throws OsadException {
        if (!rfc2822.matcher(obj).matches()) {
            throw new OsadException(message);
        }
    }
    
    public static void checkCreditCardANumber(String message, String obj) throws OsadException {
        checkNullOrEmpty("Credit card number can not be null", obj);
        if(obj.trim().length() != 16) {
            throw new OsadException(message);
        }
        for(char character : obj.toCharArray()) {
            if(!Character.isDigit(character)) {
                throw new OsadException(message);
            }
        }
    }
}
