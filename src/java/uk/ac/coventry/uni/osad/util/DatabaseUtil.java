/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.coventry.uni.osad.util;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.AnnotationConfiguration;

/**
 *
 * @author heitor
 */
public class DatabaseUtil {
    private static final String HIBERNATE_CONFIG_FILE = "hibernate.cfg.xml";
    private static SessionFactory session;
    
    public static synchronized Session getSession() {
        if(session == null) {
            session = new AnnotationConfiguration().configure(HIBERNATE_CONFIG_FILE).buildSessionFactory();
        }
        return session.openSession();
    }
    
}
