/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.coventry.uni.osad.util;

/**
 *
 * @author heitor
 */
public class OsadException extends Exception {
    
    public OsadException(String message) {
        super(message);
    }
}
