This is an e-commerce application that enable customers to shop online and also provide an administrative interface which will allow members of staff to keep track of customers’ orders and update product catalogue. This system uses JEE technology and MySQL. 

This application has the following functionalities:
	* Display Items for customers.
	* Allows customers to add items to a shopping cart. 
	* Allow removal of items from the shopping cart. 
	* Update items quantities in the shopping cart as customer add item in the cart. 
	* Customers can view a summary of all items and quantities in the shopping cart. 
	* Enable a customer to place an order and make payment providing credit card details.
	* Allow customer registration
	* Administrative interface is required to allow staff to view and tracks customers order.


Developed by: Heitor Meira (demeloh@uni.coventry.ac.uk)
You can find the repository of this project here: https://github.com/htrmeira/osad-store
